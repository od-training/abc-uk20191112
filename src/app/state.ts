import {
  createAction,
  props,
  createReducer,
  on,
  createFeatureSelector,
  createSelector
} from '@ngrx/store';
import { Video } from './dashboard/interfaces';

export const videoListArrived = createAction(
  'VIDEO_LIST_ARRIVED',
  props<{ videos: Video[] }>()
);

export interface AppState {
  dashboard: DashboardState;
}

export interface DashboardState {
  videos: Video[];
}

const initialState: DashboardState = {
  videos: []
};

export const dashboardReducer = createReducer(
  initialState,
  on(videoListArrived, (state, action) => {
    return {
      ...state,
      videos: [...action.videos]
    };
  })
);

const selectDashboard = createFeatureSelector<AppState, DashboardState>(
  'dashboard'
);

export const selectVideos = createSelector(
  selectDashboard,
  state => state.videos
);
