import { Injectable } from '@angular/core';
import {
  Actions,
  ROOT_EFFECTS_INIT,
  createEffect,
  ofType
} from '@ngrx/effects';
import { VideoDataService } from '../video-data.service';
import { switchMap, map } from 'rxjs/operators';
import { videoListArrived } from '../state';

@Injectable()
export class DashboardEffects {
  constructor(private actions$: Actions, private svc: VideoDataService) {}

  init$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ROOT_EFFECTS_INIT),
      switchMap(() => this.svc.videos$),
      map(videos => videoListArrived({ videos }))
    )
  );
}
