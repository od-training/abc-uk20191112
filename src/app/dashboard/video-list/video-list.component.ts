import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Video } from '../interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.css']
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  selectedVideoId: Observable<string>;

  constructor(route: ActivatedRoute, private router: Router) {
    this.selectedVideoId = route.queryParamMap.pipe(
      map(params => params.get('selectedId'))
    );
  }

  selectVideo(video: Video) {
    const queryParams = { selectedId: video.id };
    this.router.navigate([], { queryParams });
  }
}
