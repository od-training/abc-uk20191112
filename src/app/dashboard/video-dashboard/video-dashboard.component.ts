import { Component } from '@angular/core';
import { Video } from '../interfaces';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { Store, select } from '@ngrx/store';
import { AppState, selectVideos, videoListArrived } from 'src/app/state';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.css']
})
export class VideoDashboardComponent {
  videos$: Observable<Video[]>;

  constructor(router: Router, route: ActivatedRoute, store: Store<AppState>) {
    this.videos$ = store.pipe(
      select(selectVideos),
      tap(videos => {
        const id = route.snapshot.queryParamMap.get('selectedId');
        if (!id && videos && videos.length) {
          const queryParams = { selectedId: videos[0].id };
          router.navigate([], { queryParams });
        }
      })
    );
  }
}
