import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Video } from './dashboard/interfaces';
import { map } from 'rxjs/operators';

const url = 'https://api.angularbootcamp.com/videos';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  videos$: Observable<Video[]>;

  constructor(private http: HttpClient) {
    this.videos$ = this.http.get<Video[]>(url).pipe(
      map(videos => {
        return videos
          .filter(video => {
            return video.title.startsWith('Angular');
          })
          .map(video => {
            // Use the spread operator to clone the old
            // video and make a small change
            return {
              ...video,
              title: video.title.toUpperCase()
            };
          });
      })
    );
  }

  getVideo(id: string): Observable<Video> {
    return this.http.get<Video>(url + '/' + id);
  }
}
